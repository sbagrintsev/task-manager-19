package ru.tsc.bagrintsev.tm.exception.entity;

public class AccessDeniedException extends AbstractEntityException{

    public AccessDeniedException() {
        super("Access denied! Wrong password...");
    }

}

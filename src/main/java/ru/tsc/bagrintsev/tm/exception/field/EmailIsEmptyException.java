package ru.tsc.bagrintsev.tm.exception.field;

public final class EmailIsEmptyException extends AbstractFieldException{

    public EmailIsEmptyException() {
        super("Error! Description is empty...");
    }

}

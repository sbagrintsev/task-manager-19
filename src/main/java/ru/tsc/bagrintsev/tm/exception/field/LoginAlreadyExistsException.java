package ru.tsc.bagrintsev.tm.exception.field;

public class LoginAlreadyExistsException extends AbstractFieldException{

    public LoginAlreadyExistsException(final String login) {
        super(String.format("Error! Login %s is already in use...", login));
    }

}

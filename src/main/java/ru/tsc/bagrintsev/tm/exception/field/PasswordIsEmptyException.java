package ru.tsc.bagrintsev.tm.exception.field;

public final class PasswordIsEmptyException extends AbstractFieldException{

    public PasswordIsEmptyException() {
        super("Error! Password is empty...");
    }

}

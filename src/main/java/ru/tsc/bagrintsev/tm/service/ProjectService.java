package ru.tsc.bagrintsev.tm.service;

import ru.tsc.bagrintsev.tm.api.repository.IProjectRepository;
import ru.tsc.bagrintsev.tm.api.sevice.IProjectService;
import ru.tsc.bagrintsev.tm.enumerated.Entity;
import ru.tsc.bagrintsev.tm.enumerated.EntityField;
import ru.tsc.bagrintsev.tm.enumerated.Status;
import ru.tsc.bagrintsev.tm.exception.AbstractException;
import ru.tsc.bagrintsev.tm.exception.field.*;
import ru.tsc.bagrintsev.tm.model.Project;

import java.util.Date;

public final class ProjectService extends AbstractService<Project, IProjectRepository> implements IProjectService {

    public ProjectService(final IProjectRepository repository) {
        super(repository);
    }

    @Override
    public Project create(final String name) throws AbstractFieldException {
        check(EntityField.NAME, name);
        return repository.create(name);
    }

    @Override
    public Project create(final String name, String description) throws AbstractFieldException {
        check(EntityField.NAME, name);
        check(EntityField.DESCRIPTION, description);
        return repository.create(name, description);
    }

    @Override
    public Project updateByIndex(final Integer index, final String name, final String description) throws AbstractException {
        check(index);
        check(EntityField.NAME, name);
        final Project project = findOneByIndex(index);
        project.setName(name);
        project.setDescription(description);
        return project;
    }

    @Override
    public Project updateById(final String id, final String name, final String description) throws AbstractException {
        check(EntityField.ID, id);
        check(EntityField.NAME, name);
        final Project project = findOneById(id);
        check(Entity.PROJECT, project);
        project.setName(name);
        project.setDescription(description);
        return project;
    }

    @Override
    public Project changeProjectStatusByIndex(final Integer index, final Status status) throws AbstractException {
        check(index);
        final Project project = findOneByIndex(index);
        project.setStatus(status);
        if (status.equals(Status.IN_PROGRESS)) {
            project.setDateStarted(new Date());
        } else if (status.equals(Status.COMPLETED)) {
            project.setDateFinished(new Date());
        } else if (status.equals(Status.NOT_STARTED)) {
            project.setDateStarted(null);
            project.setDateFinished(null);
        }
        return project;
    }

    @Override
    public Project changeProjectStatusById(final String id, final Status status) throws AbstractException {
        check(EntityField.ID, id);
        final Project project = findOneById(id);
        check(Entity.PROJECT, project);
        project.setStatus(status);
        if (status.equals(Status.IN_PROGRESS)) {
            project.setDateStarted(new Date());
        } else if (status.equals(Status.COMPLETED)) {
            project.setDateFinished(new Date());
        } else if (status.equals(Status.NOT_STARTED)) {
            project.setDateStarted(null);
            project.setDateFinished(null);
        }
        return project;
    }

}

package ru.tsc.bagrintsev.tm.service;

import ru.tsc.bagrintsev.tm.api.repository.ICommandRepository;
import ru.tsc.bagrintsev.tm.api.sevice.ICommandService;
import ru.tsc.bagrintsev.tm.command.AbstractCommand;
import ru.tsc.bagrintsev.tm.exception.system.ArgumentNotSupportedException;
import ru.tsc.bagrintsev.tm.exception.system.CommandNotSupportedException;

import java.util.Collection;

public final class CommandService implements ICommandService {

    private final ICommandRepository commandRepository;


    public CommandService(ICommandRepository commandRepository) {
        this.commandRepository = commandRepository;
    }

    @Override
    public Collection<AbstractCommand> getAvailableCommands() {
        return commandRepository.getAvailableCommands();
    }

    @Override
    public void add(final AbstractCommand command) {
        if (command == null) return;
        commandRepository.add(command);
    }

    @Override
    public AbstractCommand getCommandByName(final String name) throws CommandNotSupportedException {
        if (name == null || name.isEmpty()) throw new CommandNotSupportedException(name);
        final AbstractCommand command = commandRepository.getCommandByName(name);
        if (command == null) throw new CommandNotSupportedException(name);
        return command;
    }

    @Override
    public AbstractCommand getCommandByShort(final String shortName) throws ArgumentNotSupportedException {
        if (shortName == null || shortName.isEmpty()) throw new ArgumentNotSupportedException(shortName);
        final AbstractCommand command = commandRepository.getCommandByShort(shortName);
        if (command == null) throw new ArgumentNotSupportedException(shortName);
        return command;
    }

}

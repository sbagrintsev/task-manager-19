package ru.tsc.bagrintsev.tm.service;

import ru.tsc.bagrintsev.tm.api.repository.ITaskRepository;
import ru.tsc.bagrintsev.tm.api.sevice.ITaskService;
import ru.tsc.bagrintsev.tm.enumerated.Entity;
import ru.tsc.bagrintsev.tm.enumerated.EntityField;
import ru.tsc.bagrintsev.tm.enumerated.Status;
import ru.tsc.bagrintsev.tm.exception.AbstractException;
import ru.tsc.bagrintsev.tm.exception.entity.TaskNotFoundException;
import ru.tsc.bagrintsev.tm.exception.field.*;
import ru.tsc.bagrintsev.tm.model.Task;

import java.util.Date;
import java.util.Map;

public final class TaskService extends AbstractService<Task, ITaskRepository> implements ITaskService {

    public TaskService(final ITaskRepository repository) {
        super(repository);
    }

    @Override
    public Task create(final String name) throws AbstractFieldException {
        check(EntityField.NAME, name);
        return repository.create(name);
    }

    @Override
    public Task create(final String name, String description) throws AbstractFieldException {
        check(EntityField.NAME, name);
        check(EntityField.DESCRIPTION, description);
        return repository.create(name, description);
    }

    @Override
    public Map<Integer, Task> findAllByProjectId(final String projectId) throws AbstractFieldException {
        check(EntityField.PROJECT_ID, projectId);
        return repository.findAllByProjectId(projectId);
    }

    @Override
    public Task updateByIndex(final Integer index, final String name, final String description) throws AbstractException {
        check(index);
        check(EntityField.NAME, name);
        final Task task = findOneByIndex(index);
        task.setName(name);
        task.setDescription(description);
        return task;
    }

    @Override
    public Task updateById(final String id, final String name, final String description) throws AbstractException {
        check(EntityField.TASK_ID, id);
        check(EntityField.NAME, name);
        final Task task = findOneById(id);
        check(Entity.TASK, task);
        task.setName(name);
        task.setDescription(description);
        return task;
    }

    @Override
    public Task changeTaskStatusByIndex(final Integer index, final Status status) throws AbstractException {
        check(index);
        final Task task = findOneByIndex(index);
        task.setStatus(status);
        if (status.equals(Status.IN_PROGRESS)) {
            task.setDateStarted(new Date());
        } else if (status.equals(Status.COMPLETED)) {
            task.setDateFinished(new Date());
        } else if (status.equals(Status.NOT_STARTED)) {
            task.setDateStarted(null);
            task.setDateFinished(null);
        }
        return task;
    }

    @Override
    public Task changeTaskStatusById(final String id, final Status status) throws AbstractException {
        check(EntityField.TASK_ID, id);
        final Task task = findOneById(id);
        check(Entity.TASK, task);
        task.setStatus(status);
        if (status.equals(Status.IN_PROGRESS)) {
            task.setDateStarted(new Date());
        } else if (status.equals(Status.COMPLETED)) {
            task.setDateFinished(new Date());
        } else if (status.equals(Status.NOT_STARTED)) {
            task.setDateStarted(null);
            task.setDateFinished(null);
        }
        return task;
    }

}

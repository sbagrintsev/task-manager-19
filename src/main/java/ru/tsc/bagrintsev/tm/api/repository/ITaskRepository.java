package ru.tsc.bagrintsev.tm.api.repository;

import ru.tsc.bagrintsev.tm.exception.AbstractException;
import ru.tsc.bagrintsev.tm.exception.entity.ModelNotFoundException;
import ru.tsc.bagrintsev.tm.exception.entity.TaskNotFoundException;
import ru.tsc.bagrintsev.tm.model.Task;

import java.util.Map;

public interface ITaskRepository extends IAbstractRepository<Task>{

    Task create(String name);

    Task create(String name, String description);

    Map<Integer, Task> findAllByProjectId(String projectId);

    void setProjectId(String taskId, String projectId) throws AbstractException;

}

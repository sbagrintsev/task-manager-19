package ru.tsc.bagrintsev.tm.api.sevice;

import ru.tsc.bagrintsev.tm.enumerated.Status;
import ru.tsc.bagrintsev.tm.exception.AbstractException;
import ru.tsc.bagrintsev.tm.exception.field.*;
import ru.tsc.bagrintsev.tm.model.Task;

import java.util.Map;

public interface ITaskService extends IAbstractService<Task>, IService{

    Task create(String name) throws AbstractFieldException;

    Task create(String name, String description) throws AbstractFieldException;

    Map<Integer, Task> findAllByProjectId(String projectId) throws AbstractFieldException;

    Task updateByIndex(Integer index, String name, String description) throws AbstractException;

    Task updateById(String id, String name, String description) throws AbstractException;

    Task changeTaskStatusByIndex(Integer index, Status status) throws AbstractException;

    Task changeTaskStatusById(String id, Status status) throws AbstractException;

}

package ru.tsc.bagrintsev.tm.api.sevice;

import ru.tsc.bagrintsev.tm.enumerated.Entity;
import ru.tsc.bagrintsev.tm.enumerated.EntityField;
import ru.tsc.bagrintsev.tm.exception.entity.*;
import ru.tsc.bagrintsev.tm.exception.field.*;

public interface IService {

    default void check(EntityField field, String attribute) throws AbstractFieldException {
        if (attribute == null || attribute.isEmpty()) {
            switch (field) {
                case NAME:
                    throw new NameIsEmptyException();
                case DESCRIPTION:
                    throw new DescriptionIsEmptyException();
                case ID: throw new IdIsEmptyException();
                case PROJECT_ID:
                case TASK_ID:
                    throw new IdIsEmptyException(field.getDisplayName());
                case LOGIN:
                    throw new LoginIsEmptyException();
                case EMAIL:
                    throw new EmailIsEmptyException();
                case PASSWORD:
                    throw new PasswordIsEmptyException();
            }
        }
    }

    default void check(Entity entity, Object instance) throws AbstractEntityException{
        if (instance == null) {
            switch (entity) {
                case TASK: throw new TaskNotFoundException();
                case PROJECT: throw new ProjectNotFoundException();
                case USER: throw new UserNotFoundException();
                case ABSTRACT: throw new ModelNotFoundException();
                case ROLE: throw new IncorrectRoleException();
            }
        }
    }

}

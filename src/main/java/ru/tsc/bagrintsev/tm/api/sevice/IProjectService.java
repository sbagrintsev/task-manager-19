package ru.tsc.bagrintsev.tm.api.sevice;

import ru.tsc.bagrintsev.tm.enumerated.Status;
import ru.tsc.bagrintsev.tm.exception.AbstractException;
import ru.tsc.bagrintsev.tm.exception.field.*;
import ru.tsc.bagrintsev.tm.model.Project;


public interface IProjectService extends IAbstractService<Project>, IService{

    Project create(String name) throws AbstractFieldException;

    Project create(String name, String description) throws AbstractFieldException;

    Project updateByIndex(Integer index, String name, String description) throws AbstractException;

    Project updateById(String id, String name, String description) throws AbstractException;

    Project changeProjectStatusByIndex(Integer index, Status stauts) throws AbstractException;

    Project changeProjectStatusById(String id, Status status) throws AbstractException;

}

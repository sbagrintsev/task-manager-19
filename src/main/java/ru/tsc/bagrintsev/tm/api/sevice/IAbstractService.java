package ru.tsc.bagrintsev.tm.api.sevice;

import ru.tsc.bagrintsev.tm.api.repository.IAbstractRepository;
import ru.tsc.bagrintsev.tm.enumerated.Sort;
import ru.tsc.bagrintsev.tm.exception.field.IncorrectIndexException;
import ru.tsc.bagrintsev.tm.model.AbstractModel;


import java.util.List;

public interface IAbstractService<M extends AbstractModel> extends IAbstractRepository<M>, IService {

    List<M> findAll(Sort sort);

    void check(Integer index) throws IncorrectIndexException;

}

package ru.tsc.bagrintsev.tm.api.model;

public interface IAbstractHas extends IHasDateStarted, IHasDateCreated, IHasStatus, IHasName{
}

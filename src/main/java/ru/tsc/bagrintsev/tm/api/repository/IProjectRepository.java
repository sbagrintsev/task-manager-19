package ru.tsc.bagrintsev.tm.api.repository;

import ru.tsc.bagrintsev.tm.model.Project;

public interface  IProjectRepository extends IAbstractRepository<Project>{

    Project create(String name);

    Project create(String name, String description);

}

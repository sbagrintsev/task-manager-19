package ru.tsc.bagrintsev.tm.command.system;

public class AboutCommand extends AbstractSystemCommand {

    @Override
    public void execute() {
        System.out.println("[Author]");
        System.out.println("Name: Sergey Bagrintsev");
        System.out.println("E-mail: sbagrintsev@t1-consulting.com");
    }

    @Override
    public String getName() {
        return "about";
    }

    @Override
    public String getShortName() {
        return "-a";
    }

    @Override
    public String getDescription() {
        return "Print about author.";
    }

}

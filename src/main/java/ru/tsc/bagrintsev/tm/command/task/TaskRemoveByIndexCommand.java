package ru.tsc.bagrintsev.tm.command.task;

import ru.tsc.bagrintsev.tm.exception.AbstractException;
import ru.tsc.bagrintsev.tm.util.TerminalUtil;

import java.io.IOException;

public class TaskRemoveByIndexCommand extends AbstractTaskCommand {
    @Override
    public void execute() throws IOException, AbstractException {
        System.out.println("[REMOVE TASK BY INDEX]");
        System.out.print("ENTER INDEX: ");
        final Integer index = TerminalUtil.nextNumber() - 1;
        getTaskService().removeByIndex(index);
    }

    @Override
    public String getName() {
        return "task-remove-by-index";
    }

    @Override
    public String getDescription() {
        return "Remove task by index.";
    }
}

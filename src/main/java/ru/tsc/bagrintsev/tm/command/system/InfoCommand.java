package ru.tsc.bagrintsev.tm.command.system;

import static ru.tsc.bagrintsev.tm.util.FormatUtil.formatBytes;

public class InfoCommand extends AbstractSystemCommand {

    @Override
    public void execute() {
        final Runtime runtime = Runtime.getRuntime();
        final String processors = String.format("Available processors (cores): %d", runtime.availableProcessors());
        final long freeMemoryLong = runtime.freeMemory();
        final String freeMemory = String.format("Free memory: %s", formatBytes(freeMemoryLong));
        final long maxMemoryLong = runtime.maxMemory();
        final boolean isMaximum = maxMemoryLong == Long.MAX_VALUE;
        final String maxMemoryStr = isMaximum ? "no limit" : formatBytes(maxMemoryLong);
        final String maxMemory = String.format("Maximum memory: %s", maxMemoryStr);
        final long totalMemoryLong = runtime.totalMemory();
        final String totalMemory = String.format("Total memory available to JVM: %s", formatBytes(totalMemoryLong));
        final long usedMemoryLong = totalMemoryLong - freeMemoryLong;
        final String usedMemory = String.format("Used memory in JVM: %s", formatBytes(usedMemoryLong));
        System.out.printf("%s\n%s\n%s\n%s\n%s\n", processors, maxMemory, totalMemory, freeMemory, usedMemory);
    }

    @Override
    public String getName() {
        return "info";
    }

    @Override
    public String getShortName() {
        return "-i";
    }

    @Override
    public String getDescription() {
        return "Print system info.";
    }

}

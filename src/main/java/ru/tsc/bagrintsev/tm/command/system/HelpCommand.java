package ru.tsc.bagrintsev.tm.command.system;

import ru.tsc.bagrintsev.tm.api.model.ICommand;
import ru.tsc.bagrintsev.tm.command.AbstractCommand;

import java.util.Collection;

public class HelpCommand extends AbstractSystemCommand {

    @Override
    public void execute() {
        System.out.println("[Supported commands]");
        System.out.println("[While Running                       | Command Line             ]");
        System.out.println("[------------------------------------|--------------------------]");
        Collection<AbstractCommand> repository = getCommandService().getAvailableCommands();
        for (ICommand command : repository) {
            System.out.println(command);
        }
    }

    @Override
    public String getName() {
        return "help";
    }

    @Override
    public String getShortName() {
        return "-h";
    }

    @Override
    public String getDescription() {
        return "Print application help.";
    }

}
